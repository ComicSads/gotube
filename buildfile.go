// +build mage

package main

import (
	//"fmt"
	"os"
	"os/exec"
	//"github.com/magefile/mage/mg" // mg contains helpful utility functions, like Deps
)

// Runs go test
func Test() error {
	cmd := exec.Command("go", "test")
	cmd.Env = append(os.Environ(), "CGO_ENABLED=0")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
