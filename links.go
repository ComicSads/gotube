package gotube

// 0 = Video
// 1 = Channel
// 2 = Play
// 3 = User/vanity channel
type idType int

type Link struct {
	// The ID
	ID string

	// Video/Channel/Playlist
	Type idType
}

func (t idType) String() string {
	switch int(t) {
	case 0:
		return "Video"
	case 1:
		return "Channel"
	case 2:
		return "Playlist"
	case 3:
		return "Channel/vanity"
	}
	return "Unknown"
}

func (l *Link) String() string {
	return "https://www.youtube.com/watch?v=" + l.ID
}
