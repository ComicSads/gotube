package gotube

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const info = "https://www.youtube.com/get_video_info?&video_id="

type Video struct {
	// The video id
	ID string

	// The name of the video
	Title string

	// Length of video in seconds
	Seconds int

	// Date video was published
	Date Date
}

func (v *Video) UploadDate() time.Time {
	t := time.Date(v.Date.Year, time.Month(v.Date.Month), v.Date.Day, 0, 0, 0, 0, time.UTC)
	return t
}

type Date struct {
	Year  int
	Month int
	Day   int
}

// Returns the name of the video
func (v *Video) String() string {
	return v.Title
}

// Returns the url of the video
func (v *Video) Link() Link {
	return Link{ID: v.ID, Type: 0}
}

// Returns the url of the video
func (v *Video) URL() string {
	return "https://www.youtube.com/watch?v=" + v.ID
}

// Returns the length of the video
func (v *Video) Length() time.Duration {
	s := fmt.Sprintf("%ds", v.Seconds)
	d, _ := time.ParseDuration(s)
	return d
}

// Returns a video of type Video
func NewVideo(ID string) (Video, error) {
	var vid Video
	vid.ID = ID

	s, err := getVideoInfo(ID)
	if err != nil {
		return Video{}, err
	}
	u, err := url.Parse("?" + s)
	v := u.Query()
	// Detect failure
	if v.Get("status") == "fail" {
		// Most likely the video does not exist
		decode, err := url.QueryUnescape(v.Get("reason"))
		if err != nil {
			return Video{}, err
		}
		return Video{}, fmt.Errorf("failure to get video metadata: %s", decode)
	}

	// For some reason v.Get("player_response") doesn't return the full string
	// Get json data
	r, _ := regexp.Compile("player_response=.*&")
	playerResp := r.FindString(s)
	json := playerResp

	// Parse json data
	// Video title
	vid.Title = gjson.Get(json, "videoDetails.title").String()

	// Video length
	milliseconds := gjson.Get(json, "videoDetails.lengthSeconds").String()
	if milliseconds == "" {
		milliseconds = gjson.Get(json, "streamingData.formats.0.approxDurationMs").String()
		if milliseconds == "" {
			return Video{}, fmt.Errorf("Unable to get duration of video")
		}
	}
	ms, err := strconv.Atoi(milliseconds)
	if err != nil {
		return Video{}, err
	}
	vid.Seconds = ms // 1000

	// Goquery setup
	res, err := http.Get("https://youtube.com/watch?v=" + ID)
	if err != nil {
		return Video{}, err
	}
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return Video{}, err
	}

	// Video publish date
	date := doc.Find("strong.watch-time-text").Text()
	vid.Date = formatDate(date)

	return vid, nil
}

func getVideoInfo(ID string) (string, error) {
	link := info + ID
	res, err := client.Get(link)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	// Multiple levels of encoding
	s, err := url.QueryUnescape(string(b))
	if err != nil {
		return "", err
	}
	s, err = url.QueryUnescape(s)
	if err != nil {
		return "", err
	}
	s, err = url.QueryUnescape(s)
	if err != nil {
		return "", err
	}
	return s, nil
}

func GetVideoInfo(ID string) (string, error) {
	return getVideoInfo(ID)
}

// I spent about an hour googling and couldn't find a buliting feature for this somehow
func monthToInt(m string) int {
	switch m {
	case "Jan":
		return 1
	case "Feb":
		return 2
	case "Mar":
		return 3
	case "Apr":
		return 4
	case "May":
		return 5
	case "Jun":
		return 6
	case "Jul":
		return 7
	case "Aug":
		return 8
	case "Sep":
		return 9
	case "Oct":
		return 10
	case "Nov":
		return 11
	case "Dec":
		return 12
	}
	return -1
}

func formatDate(date string) Date {
	s := date
	s = strings.TrimPrefix(s, "Premiered ")
	s = strings.TrimPrefix(s, "Published on ")
	s = strings.TrimPrefix(s, "Streamed live on ")
	s = strings.TrimPrefix(s, "Joined ")
	s = strings.Replace(s, ",", "", -1)
	var dates []string
	dates = strings.Fields(s)
	m := monthToInt(dates[0])
	var tdate Date
	year, err := strconv.Atoi(dates[2])
	if err != nil {
		return Date{}
	}
	day, err := strconv.Atoi(dates[1])
	if err != nil {
		return Date{}
	}
	tdate.Year = year
	tdate.Month = m
	tdate.Day = day
	return tdate
}
