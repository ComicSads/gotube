package gotube_test

import (
	yt "gitlab.com/ComicSads/gotube"
	"testing"
)

func TestUrl(t *testing.T) {
	urls := []struct {
		desc string
		link string
		want string
	}{
		{"Test video 1", "https://www.youtube.com/watch?v=d9GpRQdzOXs", "d9GpRQdzOXs"},
		{"Test video 2", "https://www.youtube.com/watch?v=HFF-jD6cyZY&list=PLQCGt3hQF2-ZaHyB-eGVhCMzEXZw7holG", "HFF-jD6cyZY"},
		{"Test video 3", "https://www.youtube.com/watch?v=lo24YGDJpTI", "lo24YGDJpTI"},
		{"Test channel 1", "https://www.youtube.com/channel/UCyNlm_1nr1QRqPd7FHwS91w/videos", "UCyNlm_1nr1QRqPd7FHwS91w"},
		{"Test channel 2", "https://www.youtube.com/channel/UCjdQaSJCYS4o2eG93MvIwqg", "UCjdQaSJCYS4o2eG93MvIwqg"},
		{"Test channel 3", "https://www.youtube.com/user/PewDiePie", "PewDiePie"},
		{"Test channel 4", "https://www.youtube.com/user/HIIMCAPSLOCK/videos", "HIIMCAPSLOCK"},
		{"Test channel 5", "https://www.youtube.com/TheEscapistMagazine/videos", "TheEscapistMagazine"},
		{"Test playlist 1", "https://www.youtube.com/playlist?list=PLFsQleAWXsj_4yDeebiIADdH5FMayBiJo", "PLFsQleAWXsj_4yDeebiIADdH5FMayBiJo"},
	}
	for _, test := range urls {
		t.Run(test.desc, func(t *testing.T) {
			got, err := yt.GetID(test.link)
			if err != nil {
				panic(err)
			}
			if got.ID != test.want {
				t.Errorf("\nreturned: %s\nwanted: %s\n", got.ID, test.want)
			}
		})
	}
}
