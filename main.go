package gotube

import (
	"fmt"
	"net/http"
	"strings"
)

// Specify custom client so we can change stuff if we want to
var client = &http.Client{}

func varType(x interface{}) string {
	return fmt.Sprintf("%T", x)
}

// Returns the id of a video, channel, playlist, etc.
func GetID(link string) (Link, error) {
	var t idType
	s := link
	// Remove possible prefixes
	s = strings.TrimPrefix(s, "http://")
	s = strings.TrimPrefix(s, "https://")
	s = strings.TrimPrefix(s, "youtube://")
	s = strings.TrimPrefix(s, "www.")
	s = strings.TrimPrefix(s, "youtube.com")
	s = strings.TrimPrefix(s, "/")

	// Make sure links like youtube.com/PewDiePie or youtube.com/c/PewDiePie are covered
	switch s[0:2] {
	case "us":
	case "ch":
	case "wa":
	case "pl":
	case "c/":
		s = s[2:]
		fallthrough
	default:
		s = "user/" + s
	}
	// Identify
	switch s[0:5] {
	case "user/":
		// Channel with custom link
		s = s[5:]
		t = 3
		// Remove any trailing url pages, i.e. PewDiePie/videos
		if strings.Contains(s, "/") {
			for i, _ := range s {
				if s[i] == '/' {
					s = s[0:i]
					break
				}
			}
		}
	case "chann":
		// Channel
		s = s[8:32]
		t = 1
	case "watch":
		// Video
		s = s[8:19]
		t = 0
	case "playl":
		// Playlist
		s = s[14:48]
		t = 2
	default:
		return Link{}, fmt.Errorf("unknown youtube url type: %s", link)
	}
	return Link{ID: s, Type: t}, nil
}
