package gotube

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"strconv"
	"strings"
	"time"
)

type Channel struct {
	// ID of the channel
	ID string

	// The amount of subscribers a channel has
	SubCount int

	// Date channel was created
	Date Date

	// Channel Description
	Description string
}

func (c *Channel) JoinDate() time.Time {
	t := time.Date(c.Date.Year, time.Month(c.Date.Month), c.Date.Day, 0, 0, 0, 0, time.UTC)
	return t
}

func NewChannel(ID string) (Channel, error) {
	var c Channel
	c.ID = ID
	res, err := client.Get(c.URL())
	if err != nil {
		return Channel{}, err
	}
	defer res.Body.Close()
	doc, err := goquery.NewDocumentFromReader(res.Body)
	_ = doc
	if err != nil {
		return Channel{}, err
	}
	// Get Subcount
	subs := doc.Find(".subscribed")
	if subs.Text() == "" {
		return Channel{}, fmt.Errorf("Unable to find subcount")
	}
	subcount, err := strconv.Atoi(strings.Replace(subs.Text(), ",", "", -1))
	if err != nil {
		return Channel{}, err
	}
	c.SubCount = subcount

	// Get channel creation date
	c, err = goqueryStuff(c)
	if err != nil {
		return Channel{}, err
	}
	return c, nil
}

func goqueryStuff(c Channel) (Channel, error) {
	var d Date
	res, err := client.Get(c.URL() + "/about")
	if err != nil {
		return Channel{}, err
	}
	defer res.Body.Close()
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return Channel{}, err
	}
	var joined string
	doc.Find("span.about-stat").Each(func(i int, s *goquery.Selection) {
		if strings.HasPrefix(s.Text(), "Joined") {
			joined = s.Text()
		}
	})
	if joined == "" {
		return Channel{}, fmt.Errorf("Empty string for channel creation date")
	}
	d = formatDate(joined)
	c.Date = d

	c.Description = doc.Find("div.about-description pre").Text()
	return c, nil
}

// Returns the url of the channel
func (c *Channel) Link() Link {
	return Link{ID: c.ID, Type: 1}
}

// Returns the url of the channel
func (c *Channel) URL() string {
	return "https://www.youtube.com/channel/" + c.ID
}

// Returns the most recent video uploaded by a channel
func MostRecentVideo(ID string) (Video, error) {
	s := "https://www.youtube.com/channel/" + ID + "/videos"
	res, err := client.Get(s)
	if err != nil {
		return Video{}, err
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return Video{}, err
	}

	url := doc.Find(".yt-lockup-title").First().Find("a")
	if url.Text() == "" {
		return Video{}, fmt.Errorf("Unable to find recent video")
	}
	href, ok := url.Attr("href")
	if !ok {
		fmt.Println(href)
		return Video{}, fmt.Errorf("Could not get link to recent video")
	}
	link, err := GetID(href)
	if err != nil {
		return Video{}, err
	}

	vid, err := NewVideo(link.ID)
	if err != nil {
		return Video{}, err
	}
	return vid, nil
}

// Returns the 29 most recent videos uploaded by a channel
func ChannelVideoIDs(ID string) ([]Link, error) {
	s := "https://www.youtube.com/channel/" + ID + "/videos"

	res, err := client.Get(s)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, err
	}

	var videos []Link
	doc.Find(".yt-lockup-title").Each(func(i int, s *goquery.Selection) {
		url, ok := s.Find("a").Attr("href")
		if ok {
			videos = append(videos, Link{ID: url[8:], Type: 0})
		}
	})
	return videos, nil
}

// Similar to ChannelVideoIDs, except this returns a video struct with extra data
func ChannelVideos(ID string) ([]Video, error) {
	ids, err := ChannelVideoIDs(ID)
	if err != nil {
		return nil, err
	}
	var videos []Video
	for _, v := range ids {
		vid, err := NewVideo(v.ID)
		if err != nil {
			return nil, err
		}
		videos = append(videos, vid)
	}
	return videos, nil
}
