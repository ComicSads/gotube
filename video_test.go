package gotube_test

import (
	yt "gitlab.com/ComicSads/gotube"
	"testing"
)

func TestNewVideo(t *testing.T) {
	urls := []struct {
		desc string
		id   string
		want yt.Video
	}{
		{"Test video 1", "YbJOTdZBX1g",
			yt.Video{ID: "YbJOTdZBX1g",
				Title:   "YouTube Rewind 2018: Everyone Controls Rewind | #YouTubeRewind",
				Seconds: 493,
				Date:    yt.Date{2018, 12, 6}}},

		{"Test video 2", "FtSd844cI7U",
			yt.Video{ID: "FtSd844cI7U",
				Title:   "CATS  - Official Trailer [HD]",
				Seconds: 144,
				Date:    yt.Date{2019, 7, 18}}},

		{"Test video 3", "FvvZaBf9QQI",
			yt.Video{ID: "FvvZaBf9QQI",
				Title:   "Sonic The Hedgehog (2019) - Official Trailer - Paramount Pictures",
				Seconds: 167,
				Date:    yt.Date{2019, 4, 30}}},
	}
	for _, test := range urls {
		t.Run(test.desc, func(t *testing.T) {
			got, err := yt.NewVideo(test.id)
			if err != nil {
				t.Errorf("Error in test: %s\n", err)
			}
			if got != test.want {
				t.Errorf("Test failed!\nReturned: %v\nWanted:   %v\n", got, test.want)
			}
		})
	}
}
